# Walrus on AWS

This module is used to deploy a walrus server on AWS.

## Providers

| Name | Version |
|------|---------|
| aws  | n/a     |

## Inputs

| Name                                     | Description                                |   Type   |      Default      | Required |
|------------------------------------------|--------------------------------------------|:--------:|:-----------------:|:--------:|
| walrus_image                             | Walrus image                               | `string` | `"sealio/walrus"` |    no    |
| walrus_bootstrap_password                | Walrus bootstrap password                  | `string` |    `"123456"`     |    no    |
| walrus_data_source_address               | Walrus data source address                 | `string` |       `""`        |    no    |
| walrus_casdoor_server                    | Walrus casdoor server address              | `string` |       `""`        |    no    |
| walrus_data_source_data_encryption       | Walrus data source data encryption         | `string` |       `""`        |    no    |
| walrus_kubeconfig                        | Walrus kubeconfig                          | `string` |       `""`        |    no    |
| instance\_type                           | Instance type                              | `string` |   `"t3.medium"`   |    no    |
| disk\_size                               | Root disk size in GiB                      | `number` |       `50`        |    no    |
| security\_group\_name                    | Security group Name                        | `string` |   `"all-open"`    |    no    |
| warus_metadata_application_name          | Walrus metadata application  name.         | `string` |      `"foo"`      |    no    |
| warus_metadata_application_instance_name | Walrus metadata application instance name. | `string` |      `"bar"`      |    no    |

## Outputs

| Name       | Description       |
|------------|-------------------|
| walrus_url | Walrus access URL |
