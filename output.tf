output "walrus_url" {
  description = "Walrus access url"
  value = "https://${aws_instance.walrus.public_ip}"
}