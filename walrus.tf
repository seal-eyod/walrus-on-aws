data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


data "aws_security_group" "selected" {
  name = var.security_group_name
}

data "aws_subnets" "selected" {
  filter {
    name   = "vpc-id"
    values = [data.aws_security_group.selected.vpc_id]
  }
}

resource "aws_instance" "walrus" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = data.aws_subnets.selected.ids.0
  vpc_security_group_ids = [data.aws_security_group.selected.id]
  key_name      = var.key_name
  user_data     = <<-EOF
                  #!/bin/bash
                  set -ex;
                  public_ip=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
                  curl -fsSL https://get.docker.com | bash && sudo usermod -aG docker ubuntu
                      docker_command="docker run -d --privileged --restart=always -p 80:80 -p 443:443"

                  if [ -n "${var.walrus_data_source_data_encryption}" ]; then
                    docker_command="$docker_command -e SERVER_DATA_SOURCE_DATA_ENCRYPTION=${var.walrus_data_source_data_encryption}"
                  fi

                  $docker_command -e SERVER_BOOTSTRAP_PASSWORD="${var.walrus_bootstrap_password}" \
                  -e SERVER_DATASOURCE_ADDRESS="${var.walrus_data_source_address}" \
                  -e SERVER_CASDOOR_SERVER="${var.walrus_casdoor_server}" \
                  -e SERVER_KUBECONFIG="${var.walrus_kubeconfig}" \
                  "${var.walrus_image}"
                  EOF
  tags = {
    "Name" = "${var.walrus_metadata_application_name}-${var.walrus_metadata_application_instance_name}-walrus"
  }

  root_block_device {
    volume_size = var.disk_size
  }
}

resource "null_resource" "walrus_health_check" {
  depends_on = [
    aws_instance.walrus,
  ]

  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec" {
    command     = "for i in `seq 1 60`; do curl -k -s $ENDPOINT >/dev/null && exit 0 || true; sleep 5; done; echo TIMEOUT && exit 1"
    interpreter = ["/bin/sh", "-c"]
    environment = {
      ENDPOINT = "https://${aws_instance.walrus.public_ip}"
    }
  }
}