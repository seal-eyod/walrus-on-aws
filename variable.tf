# @group "Basic"
variable "walrus_image" {
  type        = string
  description = "Walrus image"
  default     = "sealio/walrus"
}

# @group "Basic"
variable "walrus_bootstrap_password" {
  type        = string
  description = "Walrus bootstrap password"
  default     = "123456"
  sensitive   = true
}

# @group "Basic"
variable "walrus_data_source_address" {
  type        = string
  description = "Walrus data source address"
  default     = ""
}

# @group "Basic"
variable "walrus_casdoor_server" {
  type        = string
  description = "Walrus casdoor server"
  default     = ""
}

# @group "Basic"
variable "walrus_kubeconfig" {
  type        = string
  description = "Walrus kubeconfig"
  default     = ""
}

# @group "Basic"
variable "walrus_data_source_data_encryption" {
  type        = string
  description = "Walrus data source data encryption"
  default     = ""
}

# @group "AWS"
# @options ["t3.medium", "c5.xlarge"]
variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t3.medium"
}

# @group "AWS"
variable "disk_size" {
  type        = number
  description = "Root disk size in GiB"
  default     = 50
}

# @group "AWS"
variable "key_name" {
  type        = string
  description = "AWS key name"
  default     = "xueying"
}

# @group "AWS"
variable "security_group_name" {
  type        = string
  description = "Security group Name"
  default     = "all-open"
}

# @hidden
variable "walrus_metadata_application_name" {
  type        = string
  description = "Walrus metadata application name."
  default     = "foo"
}
# @hidden
variable "walrus_metadata_application_instance_name" {
  type        = string
  description = "Walrus metadata application instance name."
  default     = "bar"
}